git pull || leszedi a változásokat/brancheket bitbucketről
git push || feltölti bitbucket-re a helyi változásokat (elsőre git push -u origin branchNév)
git commit || kommitálás / utána <esc> :wq <enter> Ha kitöltötted a commit szöveget
git commit -m "Kommit szöveg" || kommitálás kommit szöveggel

git status || lekérdezheted a hasznos infókat az aktuális állapotról
git add . || Ha hozzáadsz egy/több fájlt akkor hozzáadja őket  repohoz

git checkout branch || "branch" nevű ágra váltás
git checkout -b ujBranch || létrehozza és átvált az ujBranch ágra

git branch --all || lekérdezi a brancheket 
