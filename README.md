Dokumentáció
===================


Feladat
-------------

A feladat egy olyan program létrehozása, mellyel a szóláncjátékot tudjuk játszani. Az egyik fél küld egy szót a másiknak, a másik játékosnak erre válaszolnia kell egy olyan szóval, amely kezdőbetűje egyezik az elküldött szó utolsó betűjével, ez a játék szabálya. Ha a játékos nem tud új szót mondani, akkor az ellenfél nyer.

> **Leírás:**

> - A játékot játszhatja két "manuális" játékos, vagyis élő személy, így játszhatunk barátainkkal.
> - Játszhatunk a számítógép ellen egyedül is. A gépi játékosak egy előre megadott szókinccsel rendelkezik, melyet bármikor bővíthetünk, módosíthatunk, létrehozhatunk és hozzárendelhetünk újat.
> - Ha az "exit" szót küldjük el, akkor feladtuk a játékot, tehát az ellenfél nyer.

#### <i class="icon-file"></i> Két személyes játék indítása

A játék indításához a következő parancsokat kell megadni parancssorban, attól függően, hogy hogyan szeretnénk játszani. Ha egymás ellen élő személyekkel, akkor először a java Client paranccsal elindítjuk a szervert. A szerver fogja kezelni a csatlakozó klienseket. Utána 2 külön parancssorban elindítjuk először az egyik Manuális klienst a java Kliens paranccsal, majd a másik parancssorban ugyanígy. Ezután mindkét kliens kér egy nevet a játékostól. A nevek megadása után a Szerver egy "start" üzenetet fog küldeni valamelyik játékosnak, akinek egy szót kell küldenie, ezzel megkezdődik a játék.

#### <i class="icon-file"></i> Egyszemélyes játék indítása

Erre több lehetőségünk is van. Az egyszerűbb és felhasználóbarátabb megoldás, ha a parancssorban a java "SzolancJatek" parancsot adjuk meg. Ezzel automatikusan elindítjuk a Servert, és egy manuális klienst, amivel játszani fogunk. Bekér egy nevet és kezdődhet is a gép elleni játék. a másik lehetőség, hogy 3 parancssori ablakot nyitunk meg, Külön indítva az előbbiekben leírt parancsokat, elindítva a szervert, egy manuális klienst, és egy automata klienst. java RobotClient <gépi játékos neve> <szókincset tartalmazó file> így kell indítani. 

#### > Játék menete

Szavak küldésénél arra kell figyelnünk, hogy nem tartalmazhat szóközt illetve számokat, tehát értelmes, szónak kell lennie. Amennyiben a felhasználó helytelen szót ír be, vagy már szerepelt korábban, akkor a szerver újból bekéri a szót. A játék során az elküldött szavak listája eltárolásra kerül egy külön fájlba, ezzel vissza tudjuk nézni a játék menetét.

Terv
-------------------

![Osztályterv](/Ábra.jpg)

> **Leírás:**

> A kliens osztályok csatlakoznak a szerver osztályhoz. A Server osztály tartalmazza a ServerRunnable osztályt, ami implementálja a Runnable interfészt, és a Player osztályt.
