
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;

/**
 *
 * @author 
 */
public class Server {

  int port;

  /**
   * Szerver-t indító main fv. Paraméterben megkaphatja a server által
   * használandó portot.
   *
   * pl '-p 32123'
   *
   * Paraméter nélkül a port 32123 lesz.
   *
   * @param args
   * @throws Exception
   */
  public static void main(String args[]) throws Exception {

    Server s = new Server();

    for (int i = 0; i < args.length; i += 2) {
      switch (args[i]) {
        case "-p":
          s.setPort(Integer.parseInt(args[i + 1]));
          break;
      }
    }

    s.start();
  }

  /**
   * A Server konstruktora, mely alapértelmezetten beállítja a portot a 32123-ra.
   */
  public Server() {
    this.port = 32123;
  }
  
  /**
   * A Server konstruktora, mely beállítja a portot.
   *
   * @param port erre a portra állítja be.
   */
  public Server(int port) {
    this.port = port;
  }
  
  /**
   * Lekéri a portot, és visszatér vele.
   *
   * @return port
   */
  public int getPort() {
    return port;
  }
  
  /**
   * Beállítja a portot
   *
   * @param port erre a portra.
   */
  public void setPort(int port) {
    this.port = port;
  }

  /**
   * A Szerver logikai része.
   * Elindítja a socketeket, és várakozik a kliensek csatlakozására.
   * Összepárosítja a beérkező csatlakozó játékosokat.
   * 30 mp-et várakozik csatlakozásonként, ha nem érkezik időben csatlakozó kliens, akkor leáll.
   *
   * @throws Exception
   */
  public void start() throws Exception {
    ServerSocket ssock = new ServerSocket(port);
    ssock.setSoTimeout(30000);
    System.out.println("Listening on: " + ssock.getLocalPort());
    boolean hasMoreClient;
    int howManyClient = 0;
    Socket p1 = null, p2;

    do {
      Socket sock = null;

      try {
        sock = ssock.accept();
        hasMoreClient = true;
      } catch (IOException iOException) {
        hasMoreClient = false;
      }

      if (hasMoreClient) {
        howManyClient++;
        System.out.println(howManyClient + ". client connected on port: " + sock.getPort());
        if (howManyClient % 2 == 1) {
          p1 = sock;
        } else {
          p2 = sock;
          new Thread(new ServerRunnable(p1, p2)).start();
          System.out.println("---------------------------");
        }
      }
    } while (hasMoreClient);
    System.out.println("Don't wait for more clients...");
  }
}

/**
   * Két játékos közötti kommunikációt biztosító osztály
   * implementálja a Runnable interfészt.
   *
   */
class ServerRunnable implements Runnable {

  Player p1;
  Player p2;
  File f;

  /**
   * A ServerRunnable osztály konstruktora.
   * A két socketből példányosít egy-egy Player osztályt.
   *
   * @param p1 az első játékos sockete.
   * @param p2 a második játékos sockete.
   */
  ServerRunnable(Socket p1, Socket p2) {
    this.p1 = new Player(p1);
    this.p2 = new Player(p2);
  }

  /**
   * Megvalósítja a két játékos közötti játékot.
   * Kezeli a beérkező szavakat, és válaszokat küld.
   */
  @Override
  public void run() {
    try (
            PrintWriter out1 = new PrintWriter(p1.getSock().getOutputStream(), true);
            BufferedReader in1 = new BufferedReader(new InputStreamReader(p1.getSock().getInputStream()));
            PrintWriter out2 = new PrintWriter(p2.getSock().getOutputStream(), true);
            BufferedReader in2 = new BufferedReader(new InputStreamReader(p2.getSock().getInputStream()));) {
      int rounds = 0;
      p1.setOut(out1);
      p1.setIn(in1);
      p2.setOut(out2);
      p2.setIn(in2);

      String inputLine, outputLine;

      p1.out.println(":ready");
      p2.out.println(":ready");

      p1.out.println(":name");
      p2.out.println(":name");

      p1.setName(p1.in.readLine());
      p2.setName(p2.in.readLine());

      Player actPlayer = (rounds % 2 == 0) ? p1 : p2;
      Player otherPlayer = (rounds % 2 == 0) ? p2 : p1;

      actPlayer.out.println(":hi");
      actPlayer.out.println(actPlayer.getName());
      actPlayer.out.println(otherPlayer.getName());

      otherPlayer.out.println(":hi");
      otherPlayer.out.println(otherPlayer.getName());
      otherPlayer.out.println(actPlayer.getName());

      Calendar calendar = Calendar.getInstance();
      java.util.Date now = calendar.getTime();
      java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());

      f = new File(p1.getName() + "_" + p2.getName() + "_" + currentTimestamp.getTime() + ".txt");
      if (!f.exists()) {
        f.createNewFile();
      }
      FileOutputStream oFile = new FileOutputStream(f, false);

      actPlayer.out.println(":start");
      otherPlayer.out.println(":wait");

      PrintWriter fout = new PrintWriter(oFile, true);

      while ((inputLine = actPlayer.in.readLine()) != null) {
        if (inputLine.equals(":exit")) {
          break;
        }

        fout.println(actPlayer.getName() + " " + inputLine);
        otherPlayer.out.println(":word");
        otherPlayer.out.println(inputLine);

        actPlayer.out.println(":wait");
        otherPlayer.out.println(":send");

        rounds++;
        actPlayer = (rounds % 2 == 0) ? p1 : p2;
        otherPlayer = (rounds % 2 == 0) ? p2 : p1;
      }

      actPlayer.out.println(":loose");
      otherPlayer.out.println(":win");

      System.out.println("Disconnected: " + p1.getSock().getPort());
      System.out.println("Disconnected: " + p2.getSock().getPort());

      fout.close();
      oFile.close();
      p1.getSock().close();
      p2.getSock().close();
    } catch (IOException e) {
      System.out.println(e);
    }
  }
  
  /**
   * Az osztály reprezentálja a játékosokat.
   */
  static class Player {

    private String name;
    private Socket sock;
    public PrintWriter out;
    private BufferedReader in;

    public Player() {
    }

    public Player(Socket sock) {
      this.sock = sock;
    }

    public Player(String name, Socket sock, PrintWriter out, BufferedReader in) {
      this.name = name;
      this.sock = sock;
      this.out = out;
      this.in = in;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public Socket getSock() {
      return sock;
    }

    public void setSock(Socket sock) {
      this.sock = sock;
    }

    public PrintWriter getOut() {
      return out;
    }

    public void setOut(PrintWriter out) {
      this.out = out;
    }

    public BufferedReader getIn() {
      return in;
    }

    public void setIn(BufferedReader in) {
      this.in = in;
    }

  }

}
