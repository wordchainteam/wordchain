import java.io.*;
import java.net.*;
import java.util.ArrayList;

/**
 *
 * @author 
 */
public class Client {
  
  String host;
  int port;

  /**
   * Kliens-t indító main fv. Paraméterben megkaphatja a server által
   * használandó portot és hostot.
   *
   * pl '-p 32123 -h localhost'
   *
   * Paraméter nélkül a port 32123 lesz, a host pedig a localhost.
   *
   * @param args
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {

    Client c = new Client();
    
    for (int i = 0; i < args.length; i+=2) {
      switch (args[i]) {
        case "-h":
          c.setHost(args[i+1]);
          break;
        case "-p":
          c.setPort(Integer.parseInt(args[i+1]));
          break;
      }
    }

    c.start();
  }

  /**
   * A kliens konstruktora alapértelmezetten beállítja a hostot és a portot.
   */
  public Client() {
    this.host = "localhost";
    this.port = 32123;
  }

  /**
   * A kliens konstruktora beállítja a hostot és a portot.
   *
   * @param host a szerver címe
   * @param port a szerver portja
   */
  public Client(String host, int port) {
    this.host = host;
    this.port = port;
  }
  
  /**
   * A kliens logikai része.
   * Csatlakozik a szerverhez, és fogadja a szerver által küldött üzeneteket, és a felhasználótól bekéri hozzá a válaszokat. 
   */
  public void start() {
    try (
            Socket kkSocket = new Socket(host, port);
            PrintWriter out = new PrintWriter(kkSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(kkSocket.getInputStream())
            );) {
      BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
      String fromServer, fromUser, word = null, p1Name, p2Name;
      boolean end = false;
      ArrayList<String> words = new ArrayList<>();

      System.out.println("Connected...");
      System.out.println("Waiting for the other player...");

      while (!end && (fromServer = in.readLine()) != null) {
        fromUser = null;
        switch (fromServer) {
          case ":ready":
            System.out.println("Ready...");
            break;
          case ":name":
            System.out.print("Enter your name: ");
            fromUser = stdIn.readLine();
            break;
          case ":start":
            System.out.print("Your turn - Enter a word: ");
            boolean wrong = true;

            do {
              fromUser = stdIn.readLine();
              if (fromUser.equals(":exit")) {
                wrong = false;
              } else if (!fromUser.matches("[a-zA-ZáíűőüöúóéÁÍŰŐÜÖÚÓÉ]+")) {
                System.out.println("Error - It's not a word!");
              } else {
                wrong = false;
              }
            } while (wrong);
            words.add(fromUser);
            break;
          case ":word":
            word = in.readLine();
            words.add(word);
            break;
          case ":hi":
            p1Name = in.readLine();
            p2Name = in.readLine();
            System.out.println("Hi " + p1Name + "! Your opponent's name is " + p2Name + ".");
            break;
          case ":wait":
            System.out.println("Waiting...");
            break;
          case ":win":
            System.out.println("You won!");
            end = true;
            break;
          case ":loose":
            System.out.println("You lost!");
            end = true;
            break;
          case ":send":
            System.out.print("Your turn - Enter a word for '" + word + "' : ");
            boolean wrong2 = true;

            do {
              fromUser = stdIn.readLine();
              if (fromUser.equals(":exit")) {
                wrong2 = false;
              } else if (words.contains(fromUser)) {
                System.out.println("Error - You must use a unique word!");
              } else if (!fromUser.matches("[a-zA-ZáíűőüöúóéÁÍŰŐÜÖÚÓÉ]+")) {
                System.out.println("Error - It's not a word!");
              } else if ((fromUser.length() > 0 && !fromUser.equals(":exit") && word != null && word.length() > 0 && Character.toLowerCase(fromUser.charAt(0)) != Character.toLowerCase(word.charAt(word.length() - 1)))) {
                System.out.println("Error - must start with the following letter: " + Character.toLowerCase(word.charAt(word.length() - 1)));
              } else {
                wrong2 = false;
              }
            } while (wrong2);

            words.add(fromUser);
            break;
        }

        if (fromUser != null && !fromUser.equals(":exit")) {
          out.println(fromUser);
        }
        if (fromUser != null && fromUser.equals(":exit")) {
          end = true;
        }
      }
    } catch (UnknownHostException e) {
      System.err.println("Don't know about host " + host);
      System.exit(1);
    } catch (IOException e) {
      System.err.println("Couldn't get I/O for the connection to "
              + host);
      System.exit(1);
    }
  }

  /**
   * A host lekérése.
   * @return 
   */
  public String getHost() {
    return host;
  }

  /**
   * Host beállítása
   *
   * @param host
   */
  public void setHost(String host) {
    this.host = host;
  }

  /**
   * A port lekérése.
   * @return 
   */
  public int getPort() {
    return port;
  }
  
  /**
   * A port beállítása.
   * @param port
   */
  public void setPort(int port) {
    this.port = port;
  }

  
}
