
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 
 */
public class SzolancSzimulacio {

  /**
   *
   * @param args
   * @throws Exception
   */
  public static void main(String[] args)  throws Exception {

    Participant rc1 = new Participant(new RobotClient("localhost", 32123, "szokincs1.txt", "Player1"));
    Participant rc2 = new Participant(new RobotClient("localhost", 32123, "szokincs1.txt", "Player2"));
    Participant rc3 = new Participant(new RobotClient("localhost", 32123, "szokincs1.txt", "Player3"));
    Participant rc4 = new Participant(new RobotClient("localhost", 32123, "szokincs2.txt", "Player4"));
    
    Server s = new Server();
    
    new Thread(new ServerSimRunnable(s)).start();
    
    new Thread(rc1).start();
    new Thread(rc2).start();
    new Thread(rc3).start();
    new Thread(rc4).start();
  }
}

class ServerSimRunnable implements Runnable {

  Server s;

  public ServerSimRunnable(Server s) {
    this.s = s;
  }
  
  @Override
  public void run() {
    try {
      s.start();
    } catch (Exception ex) {
      Logger.getLogger(ServerSimRunnable.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
}

class Participant implements Runnable {

  RobotClient rc;

  public Participant(RobotClient rc) {
    this.rc = rc;
  }
  
  @Override
  public void run() {
    this.rc.start();
  }
  
}