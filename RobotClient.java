import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author 
 */
public class RobotClient {

  String host;
  int port;
  String fileName;
  String playerName;
  ArrayList<String> vocabulary;

  /**
   * Kliens-t indító main fv. Paraméterben megkaphatja a server által
   * használandó portot és hostot, valamint a gépi játékos szókincsének fájlját, és a nevét.
   *
   * pl '-p 32123 -h localhost -f szokincs1.txt -n player1'
   *
   * Paraméter nélkül a port 32123 lesz, a host pedig a localhost.
   *
   * @param args
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {

    RobotClient c = new RobotClient();

    for (int i = 0; i < args.length; i += 2) {
      switch (args[i]) {
        case "-h":
          c.setHost(args[i + 1]);
          break;
        case "-p":
          c.setPort(Integer.parseInt(args[i + 1]));
          break;
        case "-f":
          c.setFileName(args[i + 1]);
          break;
        case "-n":
          c.setPlayerName(args[i + 1]);
          break;
      }
    }

    c.start();
  }

  /**
   * Robotkliens konstruktora, alapértelmezetten beállítja a gépi játékos nevét ,és szókincsét, szerver címét és portját.
   */
  public RobotClient() {
    this.vocabulary = new ArrayList<>();
    this.host = "localhost";
    this.port = 32123;
    this.playerName = "Player" + Integer.toString((new Random()).nextInt());
    this.fileName = "szokincs1.txt";
  }
  
  /**
   * Robotkliens konstruktora, beállítja a gépi játékos nevét ,és szókincsét, szerver címét és portját.
   *
   * @param host szerver címe
   * @param port szerver portja
   * @param fileName szókincs fájl neve
   * @param playerName játékos neve
   */
  public RobotClient(String host, int port, String fileName, String playerName) {
    this.vocabulary = new ArrayList<>();
    this.host = host;
    this.port = port;
    this.fileName = fileName;
    this.playerName = playerName;
  }


  /**
   * A kliens logikai része.
   * Csatlakozik a szerverhez, és fogadja a szerver által küldött üzeneteket, és válaszol rájuk. 
   */
  public void start() {
    try (
            Socket kkSocket = new Socket(host, port);
            PrintWriter out = new PrintWriter(kkSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(kkSocket.getInputStream())
            );) {

      File f = new File(this.fileName);
      FileInputStream iFile = new FileInputStream(f);
      BufferedReader iF = new BufferedReader(new InputStreamReader(iFile));

      BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
      String fromServer, fromUser, word = null, p1Name, p2Name;
      boolean end = false;
      ArrayList<String> words = new ArrayList<>();
      String temp;
      while ((temp = iF.readLine()) != null) {
        this.vocabulary.add(temp);
      }

      System.out.println("Connected...");
      System.out.println("Waiting for the other player...");

      while (!end && (fromServer = in.readLine()) != null) {
        fromUser = null;
        switch (fromServer) {
          case ":ready":
            System.out.println("Ready...");
            break;
          case ":name":
            System.out.print("Enter your name: ");
            fromUser = this.playerName;
            break;
          case ":start":
            System.out.print("Your turn - Enter a word: ");
            fromUser = (vocabulary.size() > 0) ? vocabulary.get(0) : ":exit";
            vocabulary.remove(fromUser);
            words.add(fromUser);
            break;
          case ":word":
            word = in.readLine();
            words.add(word);
            break;
          case ":hi":
            p1Name = in.readLine();
            p2Name = in.readLine();
            System.out.println("Hi " + p1Name + "! Your opponent's name is " + p2Name + ".");
            break;
          case ":wait":
            System.out.println("Waiting...");
            break;
          case ":win":
            System.out.println("You won!");
            end = true;
            break;
          case ":loose":
            System.out.println("You lost!");
            end = true;
            break;
          case ":send":
            System.out.print("Your turn - Enter a word for '" + word + "' : ");
            temp = ":exit";
            boolean l = false;
            for (int i = 0; (i < vocabulary.size()) && !l; i++) {
              if (vocabulary.get(i).charAt(0) == word.charAt(word.length() - 1)) {
                temp = vocabulary.get(i);
                vocabulary.remove(temp);
                l = !(words.contains(temp));
              }
            }
            if (l) {
              fromUser = temp;
              System.out.println("Found: " + temp);
            } else {
              fromUser = ":exit";
              System.out.println("Word Not Found");
            }
            words.add(fromUser);
            break;
        }

        if (fromUser != null && !fromUser.equals(":exit")) {
          out.println(fromUser);
        }
        if (fromUser != null && fromUser.equals(":exit")) {
          end = true;
        }
      }
    } catch (UnknownHostException e) {
      System.err.println("Don't know about host " + host);
      System.exit(1);
    } catch (IOException e) {
      System.err.println("Couldn't get I/O for the connection to "
              + host);
      System.exit(1);
    }
  }

  /**
   *
   * @return
   */
  public String getHost() {
    return host;
  }

  /**
   *
   * @param host
   */
  public void setHost(String host) {
    this.host = host;
  }

  /**
   *
   * @return
   */
  public int getPort() {
    return port;
  }

  /**
   *
   * @param port
   */
  public void setPort(int port) {
    this.port = port;
  }

  /**
   *
   * @return
   */
  public String getFileName() {
    return fileName;
  }

  /**
   *
   * @param fileName
   */
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  /**
   *
   * @return
   */
  public String getPlayerName() {
    return playerName;
  }

  /**
   *
   * @param playerName
   */
  public void setPlayerName(String playerName) {
    this.playerName = playerName;
  }

}
